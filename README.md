# Distribución de teclado

Uno de los problemas encontrados al ussar un teclado en ingles (sin la ñ y de 64 teclas) es que
se pierden algunos de los símbolos más utilizados en el desarrollo:

- <
- \>
- {
- }
- [
- ]

Esto hace que su uso para desarrollo sea más lento que lo habitual

# Teclado en Ingles Internacional

Una alternativa es el uso del teclado en **inglés internacional**. Mediante este lenguaje tenemos acceso a todos los símbolos mencionados, salvo los propios de español como la **ñ** y los **tildes**, pero pueden ser usados mediante las combinaciones con **AltGr**, por ejemplo **AltGr + ;**.

# Distribución personalizada

Una alternativa es la creación de una distribución personalizada, mediante el uso de la heerramienta [Microsoft Keyboard Layout Creator (MSKLC)](https://www.microsoft.com/en-us/download/details.aspx?id=102134)

Es esta se puede crear la distribución deseada en el teclado.

Un ejemplo de esta distribución, es la que presentamos en este epositorio, basada en **Español (españa)** y adicionando las combinaciones:

- AltGr + , para **<**
- AktGr + ., para **>**

# Creación de una Distribución

La pantalla inicial de [Microsoft Keyboard Layout Creator (MSKLC)](https://www.microsoft.com/en-us/download/details.aspx?id=102134) nos muestra un teclado y su distribución asociada de teclas asignadas, así como sus variantes presionando las teclas de _control_, _shift_, _control_ y _altgr_.

![keyboard](images/keyboard.png)

## Configuración inicial

Lo recomendable es partir de una configuración inicial, para luego modificarla según nuestra necesidad. Para eso usamos la opción de _File > Load Existing Keyboard_.

![existing](images/existing-keyboard.png)

Allí procedemos a hacer los cambios de asignación deseados. En este caso, queríamos modificar las asignaciones de teclas cuando se presiona la tecla _AltGr_.

![altgr](images/alt-gr.png)

Hecho esto, podemos elegir tecla por tecla y asignar el símbolo que queremos se escriba al presionarla.

## Generación de la Instalación

Una vez terminada la configuración deseada, podemos generar el instalador que permite su uso en cualquier instancia de Windows.

Para esto el primer paso es definir las **propiedades** de la distribución, por ejemplo su nombre que servirá para identificarlo luego al seleccionarlo.

Para esto vamos a la opción _Project > Properties_.

![properties](images/properties.png)

Terminado esta definición, podemos pasar a generar el instalador.

![setup](images/build-setup.png)

Con el ejecutable que resulta, podemos distribuirlo y utilizar la configuración creada.
